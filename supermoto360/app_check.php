<style>
table {
    width:50%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color:#fff;
}
table#t01 th	{
    background-color: black;
    color: white;
}
</style>


<?php
/* ENABLE ERROR LOGGING */
error_reporting(E_ALL);
ini_set('display_errors', '1');

/* Testing Script to check if php querying is working.  Owner - Krish. Do not edit */

$host = "localhost"; 
$user = "sm360_adm"; 
$pass = "baigan1234"; 
$db_name = "book_supermoto360";

$link = mysqli_connect($host, $user, $pass, $db_name) or die("Error " . mysqli_error($link)); 

if (!mysqli_select_db($link, $db_name)) {
    echo 'Could not select database';
    exit;
}

$query    = 'SELECT * FROM customer_dropbox';
$result = mysqli_query($link, $query);

if (!$result) {
    echo "DB Error, could not query the database\n";
    echo 'MySQL Error: ' . mysql_error();
    exit;
}

$fields_num = mysqli_num_fields($result);

echo "<table id='t01' ><tr>";
// printing table headers
for($i=0; $i<$fields_num; $i++)
{
    $field = mysqli_fetch_field($result);
    echo "<th>{$field->name}</th>";
}
echo "</tr>\n";
// printing table rows
while($row = mysqli_fetch_row($result))
{
    echo "<tr>";
    // $row is array... foreach( .. ) puts every element
    // of $row to $cell variable
    foreach($row as $cell)
        echo "<td>$cell</td>";

    echo "</tr>\n";
}
mysqli_free_result($result);

mysqli_close($link);
?>
