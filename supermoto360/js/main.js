var app = angular.module('superMotoHome', ['ngRoute']);


/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "pages/home.html", controller: "PageCtrl"})
    // Pages
    .when("/about", {templateUrl: "pages/about.html", controller: "PageCtrl"})
    .when("/customers", {templateUrl: "pages/customers.html", controller: "PageCtrl"})
    .when("/gallery", {templateUrl: "pages/gallery.html", controller: "PageCtrl"})
    .when("/contact", {templateUrl: "pages/contact.html", controller: "PageCtrl"})
    .otherwise("/404", {templateUrl: "pages/404.html", controller: "PageCtrl"});
}]);


app.controller('PageCtrl', function ($scope, $routeParams) {
  console.log("Page Controller reporting for duty.");
    /*if($routeParams.curPage){
         $('#main-navbar').addClass("navbar-blue");
     }else{
         $('#main-navbar').removeClass("navbar-blue");
     }*/
    
    $('.navHeaderCollapse').removeClass("in");
    
});