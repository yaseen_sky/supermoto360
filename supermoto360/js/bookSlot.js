	function ValidateMobNumber(fld) {
		var patternNE = /\S+/;
		var patternIN = /^\d+$/;
	  if(fld.value == "") {
		  //alert("You didn't enter a phone number.");
		  fld.value = "";
		  fld.focus();
		  return false;
	 }else if (isNaN(fld.value)) {
		  //alert("The phone number contains illegal characters.");
		  fld.value = "";
		  fld.focus();
		  return false;
	 }else if (!(fld.value.length == 10)) {
		  //alert("The phone number is the wrong length. \nPlease enter 10 digit mobile no.");
		  fld.value = "";
		  fld.focus();
		  return false;
	 }else if(!patternNE.test(fld.value)){
		 fld.value = "";
		 fld.focus();
		 document.getElementById('form_area').className = 'hide';
		 document.getElementById('thku_callout') .innerHTML = 'The spaces bug is fixed, but we left this message to THANK YOU! <br>Please write to us at supermoto360@gmail.com if you find other bugs. :-)';
		 addClass(document.getElementById('thku_callout'), "show");
		 document.getElementById('ajax_waiting').className = "hide";
		 return false;
	 }else if(!patternIN.test(fld.value)){
		  fld.value = "";
		  fld.focus();
		  return false;
	}else{
		  return true;
	 }
	}
	
	function bookSlot(){
		try{
			document.getElementById('ajax_waiting').className = "";
			var mobile = document.getElementById('mobile');
			//var mobileTxtBox = document.getElementById('mobile_textarea');
			//mobileTxtBox.className = "textArea_bg";
			if(ValidateMobNumber(mobile)){
				var httpReq = new XMLHttpRequest();
				var url = "book_slot.php";
                var promoVal =  getUrlParam('promo');
				var vars = 'mobile=' + mobile.value + '&location='+document.getElementById('location').value + '&promo='+ promoVal;
				httpReq.open("POST", url, true);	
				httpReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				
				httpReq.onreadystatechange = function() { 
					if(httpReq.readyState == 4 && httpReq.status == 200){ 
							var return_data = httpReq.responseText; 
							// alert(return_data);
							 document.getElementById('form_area').className = 'hide';
							 addClass(document.getElementById('thku_callout'), "show");
							 document.getElementById('ajax_waiting').className = "hide";
						}
				}
				httpReq.send(vars);
			}else{
			//	addClass(mobileTxtBox, "error");
				document.getElementById('ajax_waiting').className = "hide";
			}
		}catch (err){
			alert(err.message);
			document.getElementById('ajax_waiting').className = "hide";
		}
	}
	
	function addClass(ele, className){
		ele.className = ele.className+" "+className;
	}
	
	function selectNext(e) {
		if (e.keyCode == 13) {
			document.getElementById('location').focus();
		}
	}
	
	function enterSubmitForm(e) {		
		if (e.keyCode == 13) {
			bookSlot();
		}	
	}

var getUrlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}